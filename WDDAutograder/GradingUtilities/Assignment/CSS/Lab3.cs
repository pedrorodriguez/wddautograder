using System;
using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Assignment.CSS
{
    public class Lab3 : AbstractAssignment
    {
        public Lab3()
        {
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            FailTextList = new List<string>() { "Index of", "Parent Directory", "404", "Forbidden", "<HTML>", "<HEAD>" },
                            RegexList = new List<Tuple<string, string>>()
                                {
                                    Tuple.Create(@"(.|#)?[a-zA-Z]{1}[a-zA-Z0-9]*\s*\{[a-zA-Z0-9\.:;#\-\s%,]*\}", "Checks to see if your css is valid code")
                                }
                        });
            
            AssignmentTests = testList;
            Url = "web.css";
        }
        
    }
}
