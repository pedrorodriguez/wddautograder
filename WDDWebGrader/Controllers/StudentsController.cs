﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WDDAutograder.GradingUtilities;
using WDDAutograder.GradingUtilities.StudentUtilities;
using WDDWebGrader.Models;

namespace WDDWebGrader.Controllers
{
    public class StudentsController : Controller
    {
        //
        // GET: /Students/

        public ActionResult Index()
        {
            string filename = Server.MapPath("~/WDDStudents.xml");
            List<Student> students = TestRunner.ImportStudentsFromXml(filename);
            StudentsModel model = new StudentsModel();
            model.Students = students;
            return View(model);
        }

    }
}
