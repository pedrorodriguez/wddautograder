using System;
using System.Collections.Generic;
using System.Linq;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Assignment
{
    public abstract class AbstractAssignment
    {
        public string Url { get; set; }
        public AbstractAssignment()
        {
            Url = "";
        }
        public List<AssignmentTest> AssignmentTests
        {
            get
            {
                if (_assignmentTests == null || _assignmentTests.Count == 0)
                {
                    _assignmentTests = new List<AssignmentTest>();
                    _assignmentTests.Add(new AssignmentTest());
                }

                return _assignmentTests;
            }
            set { _assignmentTests = value; }
        }
        private List<AssignmentTest> _assignmentTests;
        public List<AssignmentTest> PassedTests
        {
            get
            {
                var passedTests = from t in AssignmentTests
                                  where t.TestPassed
                                  select t;
                return passedTests.ToList();
            }
        }
        public List<AssignmentTest> FailedTests
        {
            get
            {
                var failedTests = from t in AssignmentTests
                                  where t.TestPassed == false
                                  select t;
                return failedTests.ToList();
            }
        }
        public double NormalizedRatio
        {
            get
            {
                return (AssignmentTests.Count() != 0) ? Convert.ToDouble((int) PassedTests.Count())/Convert.ToDouble((int) AssignmentTests.Count()) : 1;
            }
        }
        public int Percent
        {
            get
            {
                return Convert.ToInt32(NormalizedRatio * 100);
            }
        }
    }
}
