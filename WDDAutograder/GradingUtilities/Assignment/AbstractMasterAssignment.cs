﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment
{
    public abstract class AbstractMasterAssignment
    {
        public int AssignmentId { get; set; }
        public override string ToString()
        {
            return Name;
        }
        public string Url { get; set; }
        public string Name { get; set; }
        public int TestCount
        {
            get
            {
                List<AbstractAssignment> assignments = new List<AbstractAssignment>();
                if (HtmlAssignment != null)
                    assignments.Add(HtmlAssignment);
                if (CssAssignment != null)
                    assignments.Add(CssAssignment);
                if (JsAssignment != null)
                    assignments.Add(JsAssignment);
                int count = 0;
                foreach(AbstractAssignment a in assignments)
                {
                    count = count + a.AssignmentTests.Count;
                }
                return count;
            }
        }
        public AbstractAssignment HtmlAssignment { get; set; }
        public AbstractAssignment CssAssignment { get; set; }
        public AbstractAssignment JsAssignment { get; set; }
        public int Percent
        {
            get
            {
                List<int> percents = new List<int>();
                if (HtmlAssignment != null)
                    percents.Add(HtmlAssignment.Percent);
                if (CssAssignment != null)
                    percents.Add(CssAssignment.Percent);
                if (JsAssignment != null)
                    percents.Add(JsAssignment.Percent);
                if (percents.Count == 0)
                    return 100;
                if (percents.Count == 1)
                    return percents.First();
                if (percents.Count == 2)
                    return (int)(percents[0]*.5 + percents[1]*.5);
                if (percents.Count == 3)
                    return (int) (percents[0]*.333 + percents[1]*.333 + percents[2]*.333);
                return 100;
            }
        }
        public AbstractMasterAssignment()
        {
            Url = "";
            Name = "";
            HtmlAssignment = null;
            CssAssignment = null;
            JsAssignment = null;
        }
    }
}
