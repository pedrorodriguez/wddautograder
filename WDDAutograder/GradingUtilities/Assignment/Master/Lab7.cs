﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab7 : AbstractMasterAssignment
    {
        public Lab7()
        {
            HtmlAssignment = new HTML.Exists();
            AssignmentId = 67;
            Name = "Lab 7";
            Url = "lab7";
        }
    }
}
