using System;
using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Assignment;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Spring2012
{
    public class Lab3Assignment : AbstractAssignment
    {
        public Lab3Assignment()
        {
            this.Url = "lab3";
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            ContainsTextList = new List<string>(){"<html>", "</html>", "<body>", "</body>", "<head>", "</head>"},
                            FailTextList = new List<string>() { "Index of", "Parent Directory" }
                        });
            testList.Add(new AssignmentTest()
                             {
                                 ContainsTextList = new List<string>(){"doctype"}
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create("<title>(.*?)</title>", ""),
                                         Tuple.Create("<meta name=\"keywords\"", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                 {
                                     Tuple.Create("<div(.*?)id=\"container\"(.*?)>", ""), 
                                     Tuple.Create("<div(.*?)id=\"container\"(.*?)>(.*?)<div(.*?)id=\"header\"(.*?)>(.*?)</div>(.*?)<div(.*?)id=\"body\">(.*?)</div>(.*?)<div(.*?)id=\"footer\">(.*?)</div>(.*?)</div>", "")
                                 }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create(@"<h2>(\s*?)My Favorite Link(s|)(\s*?)</h2>", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create(@"<ul\b[^>]*>([\s]*<li\b[^>]*>(.*?)<a(.*?)>(.*?)</a>(.*?)</li>)*[\s]*</ul>", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create("<table(.*?)>", ""), 
                                         Tuple.Create("<tr(.*?)>", ""), 
                                         Tuple.Create("<th(.*?)>", ""),
                                         Tuple.Create("<td(.*?)>", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create("<video(.*?)>", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create("<p class=\"footerParagraph\">", "")
                                     }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                     {
                                         Tuple.Create(@"<link rel=""stylesheet"" href=""style\.css"" type=""text/css""(\s*?)/>", "")
                                     }
                             });
            this.AssignmentTests = testList;
        }
        
    }
}
