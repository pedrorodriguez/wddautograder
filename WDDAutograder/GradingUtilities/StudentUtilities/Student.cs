using WDDAutograder.GradingUtilities.Assignment;

namespace WDDAutograder.GradingUtilities.StudentUtilities
{
    public class Student
    {
        public string Name { get; set; }
        public string ClassAccount { get; set; }
        public int Grade { get; set; }
        public bool SendGrade { get; set; }
        public AbstractMasterAssignment WDDAssignment { get; set; }
    }
}
