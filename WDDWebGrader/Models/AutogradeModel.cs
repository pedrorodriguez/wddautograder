﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WDDAutograder.GradingUtilities.StudentUtilities;

namespace WDDWebGrader.Models
{
    public class AutogradeModel
    {
        public Student Result { get; set; }
        public int PassCounter { get; set; }
        public int FailedCounter { get; set; }
        public AutogradeModel()
        {
            Result = null;
            PassCounter = 1;
            FailedCounter = 1;
        }
    }
}