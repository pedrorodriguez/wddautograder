using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Assignment.HTML
{
    public class Exists : AbstractAssignment
    {
        public Exists()
        {
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            ContainsTextList = new List<string>(){"<html>", "</html>"},
                            FailTextList = new List<string>() { "Index of", "Parent Directory", "404", "Forbidden" }
                        });
            
            this.AssignmentTests = testList;
        }
        
    }
}
