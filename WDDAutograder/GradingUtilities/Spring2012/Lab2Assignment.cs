using System;
using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Assignment;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Spring2012
{
    public class Lab2Assignment : AbstractAssignment
    {
        public Lab2Assignment()
        {
            this.Url = "lab2";
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            ContainsTextList = new List<string>(){"<html>", "</html>"},
                            FailTextList = new List<string>() { "Index of", "Parent Directory" }
                        });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string,string>>()
                                                        {
                                                            Tuple.Create(@"<b\b[^>]*>(.*?)</b>", ""), 
                                                            Tuple.Create(@"<i\b[^>]*>(.*?)</i>", "")
                                                        }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 ContainsTextList = new List<string>()
                                                 {
                                                            "src=\"http://th07.deviantart.net/fs71/PRE/i/2010/332/2/a/hellow_world_by_davisai-d33u2c9.jpg\"",
                                                            "<img"
                                                 }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                                 {
                                                     Tuple.Create("<h(1|2|3|4|5|6|7)>", ""),
                                                     Tuple.Create("</h(1|2|3|4|5|6|7)>", "")
                                                 }
                             });
            testList.Add(new AssignmentTest()
                             {
                                 RegexList = new List<Tuple<string, string>>()
                                                 {
                                                     
                                                     Tuple.Create(@"<ul\b[^>]*>([\n\r\s]*<li\b[^>]*>[\n\r\s]*<span\b[^>]*>[\n\r\s]*(.*?)[\n\r\s]*</span>[\n\r\s]*</li>)*[\n\r\s]*</ul>", "")
                                                 }
                             });
            this.AssignmentTests = testList;
        }
        
    }
}
