﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab5 : AbstractMasterAssignment
    {
        public Lab5()
        {
            HtmlAssignment = new HTML.Exists();
            AssignmentId = 64;
            Name = "Lab 5";
            Url = "lab5";
        }
    }
}
