﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab3 : AbstractMasterAssignment
    {
        public Lab3()
        {
            CssAssignment = new CSS.Lab3();
            AssignmentId = 58;
            Name = "Lab 3";
            Url = "lab3";
        }
    }
}
