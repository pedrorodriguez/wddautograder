using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using WDDAutograder.GradingUtilities.Assignment;
using WDDAutograder.GradingUtilities.StudentUtilities;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities
{
    public class TestRunner
    {
        public enum AssignmentType {Html, Css, Js}
        public List<Student> StudentList { get; set; }
        public static string BerkeleyUrl = "http://inst.eecs.berkeley.edu";
        public TestRunner()
        {
            StudentList = new List<Student>();
        }
        public TestRunner(List<Student> studentList, Type assignmentType)
        {
            this.StudentList = studentList;
            foreach (Student s in this.StudentList)
            {
                ConstructorInfo ci = assignmentType.GetConstructor(new Type[]{});
                object o = ci.Invoke(new object[] {});
                s.WDDAssignment = (AbstractMasterAssignment)o;
            }
        }
        public void GradeAssignments()
        {
            foreach (Student s in StudentList)
            {
                GradeStudentAssignment(s);
            }
        }
        public static void GradeStudentAssignment(Student s)
        {
            if (s.WDDAssignment.HtmlAssignment != null)
            {
                string html = MakeWebRequest(MakeUrlString(s.WDDAssignment, AssignmentType.Html, s.ClassAccount));
                foreach (AssignmentTest t in s.WDDAssignment.HtmlAssignment.AssignmentTests)
                {
                    RunTest(html, t);
                }
            }
            if (s.WDDAssignment.CssAssignment != null)
            {
                string css = MakeWebRequest(MakeUrlString(s.WDDAssignment, AssignmentType.Css, s.ClassAccount));
                foreach (AssignmentTest t in s.WDDAssignment.CssAssignment.AssignmentTests)
                {
                    RunTest(css, t);
                }
            }
            if (s.WDDAssignment.JsAssignment != null)
            {
                string js = MakeWebRequest(MakeUrlString(s.WDDAssignment, AssignmentType.Js, s.ClassAccount));
                foreach (AssignmentTest t in s.WDDAssignment.JsAssignment.AssignmentTests)
                {
                    RunTest(js, t);
                }
            }
            s.Grade = s.WDDAssignment.Percent;
        }
        public static void RunTest(string link, AssignmentTest t)
        {
            bool testPassed = ValidateHtmlResponse(link, t);
            if (!testPassed)
                t.TestPassed = false;
        }
        public static string MakeUrlString(AbstractMasterAssignment a, AssignmentType t, string classAccount)
        {
            string assignmentUrl = "";
            switch(t)
            {
                case AssignmentType.Html:
                    assignmentUrl = (a.HtmlAssignment.Url != "") ? "/" + a.HtmlAssignment.Url : "";
                    break;
                case AssignmentType.Css:
                    assignmentUrl = (a.CssAssignment.Url != "") ? "/" + a.CssAssignment.Url : "";
                    break;
                case AssignmentType.Js:
                    assignmentUrl = (a.JsAssignment.Url != "") ? "/" + a.JsAssignment.Url : "";
                    break;
            }
            return BerkeleyUrl
                   + "/~" + classAccount
                   + "/" + a.Url
                   + assignmentUrl;
        }
        public static string MakeWebRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            Stream resStream;
            try
            {
                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                resStream = response.GetResponseStream();
            }
            catch(WebException e)
            {
                return "FileNotFound/Forbidden";
            }            
            string tempString = null;
            byte[] buf = new byte[8192];
            StringBuilder sb = new StringBuilder();
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count !=0)
                {
                    tempString = Encoding.ASCII.GetString(buf, 0, count);
                    sb.Append(tempString);
                }
            }
            while (count > 0);
            return sb.ToString();
        }
        public static bool ValidateHtmlResponse(string html, AssignmentTest test)
        {
            bool passes = true;
            if (html == "FileNotFound/Forbidden")
                return false;
            foreach(string t in test.ContainsTextList)
            {
                passes = passes && html.ToLower().Contains(t.ToLower());
            }
            foreach(Tuple<string, string> t in test.RegexList)
            {
                passes = passes &&
                         System.Text.RegularExpressions.Regex.IsMatch(html, t.Item1,
                                                                      System.Text.RegularExpressions.RegexOptions.
                                                                          IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
            }
            foreach(string t in test.FailTextList)
            {
                passes = passes && !html.ToLower().Contains(t.ToLower());
            }
            foreach(Delegate d in test.ValidateMethodList)
            {
                
            }
            return passes;
        }
        public static List<Student> ImportStudentsFromXml(string filename = "WDDStudents.xml", bool isWebServer = false)
        {
            XDocument xDoc = XDocument.Load(filename);
            var students = from student in xDoc.Descendants("Student")
                           select new Student()
                                      {
                                          ClassAccount = student.Element("ClassAccount").Value,
                                          Name = student.Element("Name").Value
                                      };
            return students.ToList();
        }
    }
}
