using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Assignment;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Spring2012
{
    public class Lab5Assignment : AbstractAssignment
    {
        public Lab5Assignment()
        {
            this.Url = "lab5";
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            ContainsTextList = new List<string>(){"<html>", "</html>", "<p>", "</p>"},
                            FailTextList = new List<string>() { "Index of", "Parent Directory", "1.\r\n2.\r\n3.\r\n4.\r\n" }
                        });
            
            this.AssignmentTests = testList;
        }
        
    }
}
