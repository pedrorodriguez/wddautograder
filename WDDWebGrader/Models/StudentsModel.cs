﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WDDAutograder.GradingUtilities.StudentUtilities;

namespace WDDWebGrader.Models
{
    public class StudentsModel
    {
        public List<Student> Students { get; set; }
    }
}