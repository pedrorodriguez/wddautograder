﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab4 : AbstractMasterAssignment
    {
        public Lab4()
        {
            HtmlAssignment = new HTML.Exists();
            AssignmentId = 60;
            Name = "Lab 4";
            Url = "lab4";
        }
    }
}
