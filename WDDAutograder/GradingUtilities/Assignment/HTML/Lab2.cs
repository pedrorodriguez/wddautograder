using System.Collections.Generic;
using WDDAutograder.GradingUtilities.Tests;

namespace WDDAutograder.GradingUtilities.Assignment.HTML
{
    public class Lab2 : AbstractAssignment
    {
        public Lab2()
        {
            List<AssignmentTest> testList = new List<AssignmentTest>();
            testList.Add(new AssignmentTest()
                        {
                            ContainsTextList = new List<string>(){"<html>", "</html>"},
                            FailTextList = new List<string>() { "Index of", "Parent Directory", "404", "Forbidden" }
                        });
            testList.Add(new AssignmentTest()
                             {
                                 ContainsTextList = new List<string>() { "<h1>", "<h2>", "<p>", "<strong>" }
                             });
            testList.Add(new AssignmentTest()
            {
                ContainsTextList = new List<string>() { "<head>", "<title>", "DOCTYPE" }
            });
            testList.Add(new AssignmentTest()
            {
                ContainsTextList = new List<string>() { "<ul>", "<li>", "<ol>" }
            });
            testList.Add(new AssignmentTest()
            {
                ContainsTextList = new List<string>() { "<table", "<tr>", "<td>" }
            });
            testList.Add(new AssignmentTest()
            {
                ContainsTextList = new List<string>() { "<form", "<textarea", "<input", "type=\"radio\"", "type=\"submit\"" }
            });
            this.AssignmentTests = testList;
        }
        
    }
}
