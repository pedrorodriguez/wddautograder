﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab6 : AbstractMasterAssignment
    {
        public Lab6()
        {
            HtmlAssignment = new HTML.Exists();
            AssignmentId = 66;
            Name = "Lab 6";
            Url = "lab6";
        }
    }
}
