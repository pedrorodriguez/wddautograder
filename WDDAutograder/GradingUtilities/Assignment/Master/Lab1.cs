﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab1 : AbstractMasterAssignment
    {
        public Lab1()
        {
            HtmlAssignment = new HTML.Lab1();
            AssignmentId = 55;
            Name = "Lab 1";
            Url = "";
        }
    }
}
