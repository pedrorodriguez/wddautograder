using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using WDDAutograder.GradingUtilities.StudentUtilities;

namespace WDDAutograder.GradingUtilities
{
    public class GradeSender
    {
        public static string CredentialCookie =
            "5b04262e304799d72b845f8fd21a783f2e089fd9eb404ab4742113239f4c820474aa6454dc3de34d38d73dd4bbdf15bd56eb76cf6b3525f87b7ec4497b5d4326%3A%3A88";
        public List<Student> Students { get; set; }
        public string GradeUrl { get; set; }
        public string AssignmentBindingUrl { get; set; }
        public GradeSender()
        {
            Students = new List<Student>();
            GradeUrl = "http://webdesigndecal.com/assignments/autograder";
            AssignmentBindingUrl = "";
        }
        public void SendGrades(bool sendAll)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://Webdesigndecal.com");
            CookieContainer cookies = new CookieContainer();
            cookies.Add(new Uri("http://Webdesigndecal.com"), new Cookie("user_credentials", CredentialCookie));
            var checkedStudents = from s in Students
                                 where s.SendGrade
                                 select s;
            var studentsUsed = (sendAll) ? Students : checkedStudents;
            foreach (Student s in studentsUsed)
            {
                HttpWebRequest gradeRequest =
                    (HttpWebRequest)
                    WebRequest.Create(GradeUrl + "?assignment=" + s.WDDAssignment.AssignmentId + "&grade=" + s.Grade +
                                        "&login=" + s.ClassAccount);
                gradeRequest.Method = "POST";
                gradeRequest.CookieContainer = cookies;
                HttpWebResponse gradeResponse = (HttpWebResponse) gradeRequest.GetResponse();
                gradeResponse.Close();
            }
        }
    }
}
