﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WDDAutograder.GradingUtilities;
using WDDAutograder.GradingUtilities.Assignment;
using WDDAutograder.GradingUtilities.Assignment.Master;
using WDDAutograder.GradingUtilities.StudentUtilities;
using WDDWebGrader.Models;

namespace WDDWebGrader.Controllers
{
    public class AutogradeController : Controller
    {
        //
        // GET: /Autograde/

        public ActionResult Index(string assignmentName, string classAccount)
        {
            Student s = new Student();
            if (assignmentName == "lab1")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab1();
                TestRunner.GradeStudentAssignment(s);
            } else if (assignmentName == "lab2")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab2();
                TestRunner.GradeStudentAssignment(s);
            } else if (assignmentName == "lab3")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab3();
                TestRunner.GradeStudentAssignment(s);
            } else if (assignmentName == "lab4")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab4();
                TestRunner.GradeStudentAssignment(s);
            }
            else if (assignmentName == "lab5")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab5();
                TestRunner.GradeStudentAssignment(s);
            }
            else if (assignmentName == "lab6")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab6();
                TestRunner.GradeStudentAssignment(s);
            }
            else if (assignmentName == "lab7")
            {
                s.ClassAccount = classAccount;
                s.WDDAssignment = new Lab7();
                TestRunner.GradeStudentAssignment(s);
            }
            AutogradeModel am = new AutogradeModel();
            am.Result = s;
            return View(am);
        }

    }
}
