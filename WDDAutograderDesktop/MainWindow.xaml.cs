﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using WDDAutograder.GradingUtilities;
using WDDAutograder.GradingUtilities.StudentUtilities;

namespace WDDAutograderDesktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Type> AssignmentTypes
        {
            get
            {
                Assembly assembly = Assembly.GetAssembly(typeof(WDDAutograder.GradingUtilities.GradeSender));
                var types = (from t in assembly.GetTypes()
                            where t.Namespace == "WDDAutograder.GradingUtilities.Assignment.Master"
                            && t.Name != "AbstractAssignment" 
                            select t).ToList();
                return types;
            }
        }

        public bool Graded = false;
        public List<Student> LoadedStudents = new List<Student>();
        public MainWindow()
        {
            InitializeComponent();
            AssignmentSelect.ItemsSource = (from a in AssignmentTypes
                                           select a.Name).ToList();
        }
        private void StudentSource_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".txt";
            Nullable<bool> result = dlg.ShowDialog();
            if(result == true)
            {
                string filename = dlg.FileName;
                StudentSource.Text = filename;
                FromFile.IsChecked = true;
            }
        }
        private void StudentLoad_Click(object sender, EventArgs e)
        {
            if ((bool)FromFile.IsChecked)
            {
                LoadedStudents = TestRunner.ImportStudentsFromXml(StudentSource.Text);
            }
            StudentGrid.DataContext = LoadedStudents;
        }

        private void BrowseAssignment_Click(object sender, RoutedEventArgs e)
        {
            if (StudentGrid.SelectedIndex == -1)
            {
                GradePopupLabel.Content = "Select a student assignment to view";
                GradePopup.IsOpen = true;
            }
            else if (!Graded)
            {
                GradePopupLabel.Content = "Assigment must be graded before viewing";
                GradePopup.IsOpen = true;
            }
            else
            {
                AssignmentBrowser.Navigate(TestRunner.BerkeleyUrl + "/~" + ((Student)StudentGrid.Items[StudentGrid.SelectedIndex]).ClassAccount + "/" + ((Student)StudentGrid.Items[StudentGrid.SelectedIndex]).WDDAssignment.Url);
            }
        }

        private void GradeButton_Click(object sender, RoutedEventArgs e)
        {
            if (AssignmentSelect.SelectedIndex != -1)
            {
                TestRunner runner = new TestRunner(LoadedStudents, AssignmentTypes[AssignmentSelect.SelectedIndex]);
                runner.GradeAssignments();
                StudentGrid.DataContext = new List<Student>(runner.StudentList);
                LoadedStudents = runner.StudentList;
                Graded = true;
            }
            else
            {
                GradePopupLabel.Content = "Select an assignment to grade";
                GradePopup.IsOpen = true;
            }
        }

        private void SendGrades_Click(object sender, RoutedEventArgs e)
        {
            GradeSender gs = new GradeSender();
            gs.Students = LoadedStudents;
            gs.AssignmentBindingUrl = GradingLink.Text;
            gs.SendGrades(false);
        }

        private void SendAllGrades_Click(object sender, RoutedEventArgs e)
        {
            GradeSender gs = new GradeSender();
            gs.Students = LoadedStudents;
            gs.AssignmentBindingUrl = GradingLink.Text;
            gs.SendGrades(true);
        }
    }
}
