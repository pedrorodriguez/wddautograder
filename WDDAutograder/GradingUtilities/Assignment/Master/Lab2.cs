﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WDDAutograder.GradingUtilities.Assignment.Master
{
    public class Lab2 : AbstractMasterAssignment
    {
        public Lab2()
        {
            HtmlAssignment = new HTML.Lab2();
            AssignmentId = 56;
            Name = "Lab 2";
            Url = "lab2";
        }    
    }
}
