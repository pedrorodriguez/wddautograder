using System;
using System.Collections.Generic;

namespace WDDAutograder.GradingUtilities.Tests
{
    public class AssignmentTest
    {
        public bool TestPassed { get; set; }
        public List<string> ContainsTextList { get; set; }
        public List<Tuple<string, string>> RegexList { get; set; }
        public List<string> FailTextList { get; set; } 
        public List<Delegate> ValidateMethodList { get; set; }
        public AssignmentTest()
        {
            TestPassed = true;
            ContainsTextList = new List<string>();
            RegexList = new List<Tuple<string, string>>();
            FailTextList = new List<string>();
            ValidateMethodList = new List<Delegate>();
        }
        public AssignmentTest(bool TestPassed, List<string> ContainsTextList, List<Tuple<string,string>> RegexList, List<Delegate> ValidateMethodList, List<string> FailTextList)
        {
            this.TestPassed = TestPassed;
            this.ContainsTextList = ContainsTextList;
            this.RegexList = RegexList;
            this.ValidateMethodList = ValidateMethodList;
            this.FailTextList = FailTextList;
        }
    }
}
